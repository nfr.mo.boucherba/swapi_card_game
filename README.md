# SWAPI_CARD_GAME

SWAPI_CARD_GAME

|SWAPI_CARD_GAME|Philippe Pereira - 14.01.2022|
----------------|--------------------|
|Vous êtes chargé de développer un jeu de carte sur le thème star wars|
|HTML5 - Bootstrap - Responsive design|JavaScript - JSON|

# Contexte du projet
Le but du jeu est de collectionner toutes les cartes.

On ne peut gagner une carte aléatoire que toutes les 2 heures.

Un compte a rebours indique le temps restant avant de pouvoir réclamer une carte.

Creer une animation quand on recupere une carte (soyez créatif !!)

Les cartes en doubles ne sont pas sauvegardées.

Les cartes obtenues seront sauvegardées dans le LocalStorage

Le site contiendra 2 pages:

Une page montrant le compte a rebours, et un bouton permettant de réclamer une carte
Une page permettant de consulter les cartes déja obtenue, d'en connaitre le nombre
Vous utiliserez l'api Swapi https://swapi.dev/

Il existe plusieurs categories dans swapi, seules celle ci nous interressent:

people
planets
species
starships
vehicules
Creer un wireframe avant de commencer a coder

Afin de permettre une évolution ultérieure, le site devra être développé avec le framework css Bootstrap 5.

Bonus: transformer cette app en PWA

Bonus2: chaque carte contient une image



# Modalités pédagogiques
Travail individuel Commit réguliers en respectant la spécification de Conventional Commits

# Critères de performance
Le site correspond au wireframe
Le compte a rebours fonctionne correctement
La récupération des cartes fonctionne
La sauvegarde des cartes en localStorage fonctionne
Le site est développé avec Bootstrap 5
Le site est adaptatif (responsive desktop / mobile)
Le site est en ligne
Les commits sont réguliers et explicites

# Modalités d'évaluation
Correction par le formateur

# Livrables
- Dépôt gitlab
- Site en ligne

# Template Webpack Typescript

## Requirements

[Node.js](https://nodejs.org) is required to install dependencies and run scripts via `npm`.

## Writing Code

Download the repo, **DON'T CLONE** it, run `npm install` from your project directory. Then, you can start the local development
server by running `npm start`.

After starting the development server with `npm start`, you can edit any files in the `src` folder
and webpack will automatically recompile and reload your server (available at `http://localhost:8080`
by default).

## Available Commands

| Command | Description |
|---------|-------------|
| `npm install` | Install project dependencies |
| `npm start` | Build project and open web server running project |
| `npm run build` | Builds code bundle with production settings (minification, uglification, etc..) |

# taches : local storage js pour le counter
voila